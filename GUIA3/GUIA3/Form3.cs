﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA3
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            double a = 0, b = 0, c = 0;
            try
            {
                a = Double.Parse(txt1.Text);
            }
            catch
            {
                MessageBox.Show(this, "El valor de A no es válido");
            }

            try
            {
                b = Double.Parse(txt2.Text);
            }
            catch
            {
                MessageBox.Show(this, "El valor de B no es válido");
            }

            try
            {
                c = Double.Parse(txt3.Text);
            }
            catch
            {
                MessageBox.Show(this, "El valor de C no es válido");
            }

            double discriminante = 0;
            discriminante = Math.Pow(b, 2) - 4*a*c;
            if (discriminante > 0)
            {
                MessageBox.Show(this, "La respuesta es imaginaria");
            } else
            {
                lblRes1.Text = "X1: " + ((-b + Math.Sqrt(discriminante)) / 2 * a);
                lblRes2.Text = "X2: " + ((-b - Math.Sqrt(discriminante)) / 2 * a);
            }
        }
    }
}
