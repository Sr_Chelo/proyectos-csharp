﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA6_LS192212
{
    public partial class VerAlumno : Form
    {
        Alumno alu;
        public VerAlumno(Alumno al)
        {
            InitializeComponent();
            alu = al;
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }
        private void llenar_notas()
        {
            float[] notas = alu.Calificaciones;
            dgv1.Rows.Clear();
            dgv1.Columns.Clear();
            dgv1.AutoGenerateColumns = false;
            for (int i = 0; i < notas.Length; i++)
            {
                dgv1.Columns.Add(i.ToString(), "#" + (i + 1).ToString());
                dgv1.Columns[i].Width = 80;
            }
            DataGridViewRow row = new DataGridViewRow();
            dgv1.Rows.Add(row);
            for (int i = 0; i < notas.Length; i++)
            {
                dgv1.Rows[0].Cells[i].Value = notas[i];
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void VerAlumno_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void VerAlumno_Load(object sender, EventArgs e)
        {
            txtNombre.Text = alu.Nombre;
            txtApellido.Text = alu.Apellido;
            txtCarnet.Text = alu.Carnet;
            txtMateria.Text = alu.Materia;
            llenar_notas();
        }
    }
}
