﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA6_LS192212
{
    public partial class AddAlumno : Form
    {
        public Alumno alumno;
        float[] notas = new float[0];
        public bool correct { get; set; }
        public AddAlumno()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.DoubleBuffered = true;
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void AddAlumno_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            correct = false;
            this.Close();
        }
        private void llenar_notas()
        {
            dgv1.Rows.Clear();
            dgv1.Columns.Clear();
            dgv1.AutoGenerateColumns = false;
            for (int i = 0; i < notas.Length; i++)
            {
                dgv1.Columns.Add(i.ToString(),"#"+(i+1).ToString());
                dgv1.Columns[i].Width = 80;
            }
            DataGridViewRow row = new DataGridViewRow();
            dgv1.Rows.Add(row);
            for (int i = 0; i < notas.Length; i++)
            {
                dgv1.Rows[0].Cells[i].Value = notas[i];
            }
            
        }
        private bool validar_numero(TextBox txt)
        {
            try
            {
                float value = float.Parse(txt.Text);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool validar_texto(string text)
        {
            if(text.Trim().Length == 0)
            {
                return false;
            }
            return true;
        }
        private void button3_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            if (notas.Length < 5)
            {
                if (validar_numero(txtNota))
                {
                    float nota = float.Parse(txtNota.Text);
                    if(nota>0 && nota <= 10)
                    {
                        Array.Resize(ref notas, notas.Length + 1);
                        notas[notas.Length - 1] = nota;
                        llenar_notas();
                    } else
                    {
                        errorProvider1.SetError(button3, "El valor de la nota debe ser mayor que 0 y menor o igual que 10");
                    }
                } else
                {
                    errorProvider1.SetError(button3, "Data añadido erroneo");
                }
            }
            txtNota.Focus();
            txtNota.Text = "";
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            alumno = new Alumno();
            bool state = true;
            Regex reg = new Regex("^[A-Z]{2}[0-9]{6}$");
            if (validar_texto(txtCarnet.Text) && reg.IsMatch(txtCarnet.Text))
            {
                alumno.Carnet = txtCarnet.Text.Trim();
            }
            else
            {
                state = false;
                errorProvider1.SetError(txtCarnet, "Carnet ingresdo no válido");
            }
            if (validar_texto(txtNombre.Text))
            {
                alumno.Nombre = txtNombre.Text.Trim();
            } else
            {
                state = false;
                errorProvider1.SetError(txtNombre, "Nombre ingresdo no válido");
            }

            if (validar_texto(txtApellido.Text))
            {
                alumno.Apellido = txtApellido.Text.Trim();
            } else
            {
                state = false;
                errorProvider1.SetError(txtApellido, "Apellido ingresdo no válido");
            }
            if (validar_texto(txtMateria.Text))
            {
                alumno.Materia = txtMateria.Text.Trim();
            }
            else
            {
                state = false;
                errorProvider1.SetError(txtMateria, "Materia ingresda no válida");
            }

            if(notas.Length == 0)
            {
                state = false;
                errorProvider1.SetError(button3, "No ha ingresado notas al sistema");
            } else
            {
                alumno.Calificaciones = notas;
            }
            if (state)
            {
                correct = true;
                DialogResult = DialogResult.OK;
                this.Close();
            }
        }
    }
}
