﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUIA6_LS192212
{
    public class Alumno
    {
        private string carnet;
        private string nombre;
        private string apellido;
        private string materia;
        private float[] calificaciones = new float[3];
        public string Carnet { 
            get { return carnet; } 
            set { carnet = value; }
        }
        public string Nombre {
            get { return nombre; }
            set { nombre = value; }
        }
        public string Apellido {
            get { return apellido; }
            set { apellido = value; }
        }
        public string Materia {
            get { return materia; }
            set { materia = value; }
        }
        public float[] Calificaciones {
            get { return calificaciones; }
            set { calificaciones = value; }
        }
    }
}
