﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA6_LS192212
{
    
    public partial class Form2 : Form
    {
        List<Alumno> Alumnos = new List<Alumno>();
        public Form2()
        {
            InitializeComponent();
            this.TransparencyKey = Color.Crimson;
            this.BackColor = Color.Crimson;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            string[] Nombres = { "Marcelo", "Leticia", "Alonso", "Rodrigo" };
            string[] Apellidos = { "Salzar", "Juarez", "Calles", "Figueroa" };
            string[] carnet = { "MS192212", "JS192213", "AC192214", "RF192215" };
            string[] materia = { "POO", "Diseño de Interiores", "Fotografía Profesional", "Fotografía Profesional" };
            float[] notas = { 10, 10, 10, 10 };
            for (int i = 0; i < Nombres.Length; i++)
            {
                Alumno alu = new Alumno();
                alu.Nombre = Nombres[i];
                alu.Apellido = Apellidos[i];
                alu.Carnet = carnet[i];
                alu.Materia = materia[i];
                alu.Calificaciones = notas;
                Alumnos.Add(alu);
            }
            llenar_grid();
        }
        private void llenar_grid()
        {
            dgv1.DataSource = null;
            dgv1.DataSource = Alumnos;
        }
    }
}
