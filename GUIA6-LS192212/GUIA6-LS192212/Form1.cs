﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA6_LS192212
{
    public partial class Form1 : Form
    {
        List<Alumno> Alumnos = new List<Alumno>();
        public Form1()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.DoubleBuffered = true;
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] Nombres = { "Marcelo","Leticia","Alonso", "Rodrigo"};
            string[] Apellidos = { "Salzar", "Juarez", "Calles", "Figueroa"};
            string[] carnet = { "MS192212", "JS192213", "AC192214", "RF192215"};
            string[] materia = { "POO","Diseño de Interiores", "Fotografía Profesional", "Fotografía Profesional" };
            float[] notas = { 10, 10, 10, 10 };
            for( int i = 0; i < Nombres.Length; i++)
            {
                Alumno alu = new Alumno();
                alu.Nombre = Nombres[i];
                alu.Apellido = Apellidos[i];
                alu.Carnet = carnet[i];
                alu.Materia = materia[i];
                alu.Calificaciones = notas;
                Alumnos.Add(alu);
            }
            llenar_grid();
        }
        private void llenar_grid()
        {
            dgv1.DataSource = null;
            dgv1.DataSource = Alumnos;
            dgv1.Columns[3].Width = 200;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            AddAlumno frm = new AddAlumno();
            if(frm.ShowDialog() == DialogResult.OK)
            {
                if (frm.correct)
                {
                    Alumnos.Add(frm.alumno);
                    llenar_grid();
                    MessageBox.Show(this, "Alumno ingresado correctamente");
                } else
                {
                    MessageBox.Show(this, "No se ingresó ningún alumno");
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int index = dgv1.CurrentCell.RowIndex;
            Alumnos.RemoveAt(index);
            llenar_grid();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int index = dgv1.CurrentCell.RowIndex;
            VerAlumno frm = new VerAlumno(Alumnos[index]);
            if(frm.ShowDialog() == DialogResult.OK)
            {

            }
        }
    }
}
