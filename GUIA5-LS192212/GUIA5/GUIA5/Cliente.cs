﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUIA5
{
    public class Cliente
    {
        private string dui;
        private string nombre;
        private string apellido;
        private string nit;
        private string tcuenta;
        private string ncuenta;
        private double monto;
        private string sucursal;


        public string DUI
        {
            get { return dui; }
            set { dui = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }
        public string NIT
        {
            get { return nit; }
            set { nit = value; }
        }
        public string TCuenta
        {
            get { return tcuenta; }
            set { tcuenta = value; }
        }
        public string NCuenta
        {
            get { return ncuenta; }
            set { ncuenta = NCuentaV(value); }
        }
        public double Monto
        {
            get { return monto; }
            set { monto = value; }
        }
        public string Sucursal
        {
            get { return sucursal; }
            set { sucursal = value; }
        }

        private string NCuentaV(string ncuenta)
        {
            string text = "";
            for(int i = 0; i < (5 - ncuenta.Length); i++)
            {
                text += "0";
            }
            text += ncuenta;
            ncuenta = text;
            if(tcuenta == "Corriente")
            {
                return "CC-" + ncuenta;
            } else if(tcuenta == "Ahorros")
            {
                return "CA-" + ncuenta;
            } else if(tcuenta == "A Plazos")
            {
                return "CP-" + ncuenta;
            } else
            {
                return ncuenta;
            }
        }
    }
}
