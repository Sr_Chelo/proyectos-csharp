﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA5
{
    public partial class Form2 : Form
    {
        public Cliente cliente = new Cliente();
        public bool correcto { get; set; }
        public Form2()
        {
            InitializeComponent();
            cboCuentaT.SelectedIndex = 0;
            cboSucursal.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            errorProvider1.Clear();
            bool state = true;
            if (!validar_dui(txtDui.Text))
            {
                state = false;
                errorProvider1.SetError(txtDui, "El formato de DUI no es correcto, ########-#");
            }
            if (!validar_texto(txtNombre.Text))
            {
                state = false;
                errorProvider1.SetError(txtNombre, "Debe ingresar un nombre");
            }
            if (!validar_texto(txtApellido.Text))
            {
                state = false;
                errorProvider1.SetError(txtApellido, "Debe ingresar un apellido");
            }
            if (!validar_nit(txtNit.Text))
            {
                state = false;
                errorProvider1.SetError(txtNit, "El formato de NIT no es correcto, ####-######-###-#");
            }
            if (!validar_cuenta(txtCuenta.Text))
            {
                state = false;
                errorProvider1.SetError(txtCuenta, "El formato de cuenta no es correcto");
            }
            if (!validar_numero(txtMonto.Text))
            {
                state = false;
                errorProvider1.SetError(txtMonto, "El monto ingresado es incorrecto");
            }

            if (state)
            {
                cliente.DUI = txtApellido.Text.Trim();
                cliente.Nombre = txtNombre.Text.Trim();
                cliente.Apellido = txtApellido.Text.Trim();
                cliente.NIT = txtNit.Text.Trim();
                cliente.TCuenta = cboCuentaT.Text;
                cliente.NCuenta = txtCuenta.Text.Trim();
                cliente.Monto = Double.Parse(txtMonto.Text);
                cliente.Sucursal = cboSucursal.Text;
                DialogResult = DialogResult.OK;
                correcto = true;
                MessageBox.Show(this, "Cliente Ingresado Satisfactoriamente");
            }


        }
        private bool validar_texto(string texto)
        {
            texto = texto.Trim();
            if(texto.Length < 1)
            {
                return false;
            }
            return true;
        }
        private bool validar_numero(string texto)
        {
            if (!validar_texto(texto))
            {
                return false;
            }
            try
            {
                double num = Double.Parse(texto);
                return true;
            }
            catch
            {
                return false;
            }
        }
        private bool validar_dui(string texto)
        {
            if (!validar_texto(texto))
            {
                return false;
            }
            Regex patron = new Regex("^\\d{8}-\\d$");
            if (!patron.IsMatch(texto))
            {
                return false;
            }
            return true;
        }
        private bool validar_nit(string texto)
        {
            if (!validar_texto(texto))
            {
                return false;
            }
            Regex patron = new Regex("^\\d{4}-\\d{6}-\\d{3}-\\d$");
            if (!patron.IsMatch(texto))
            {
                return false;
            }
            return true;
        }
        private bool validar_cuenta(string texto)
        {
            if (!validar_texto(texto))
            {
                return false;
            }
            try
            {
                int num = int.Parse(texto);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

