﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private List<Cliente> Clientes = new List<Cliente>();

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 frm = new Form2();
            if(frm.ShowDialog() == DialogResult.OK)
            {
                if (!frm.correcto)
                {
                    MessageBox.Show(this, "No se ha ingresado el cliente");
                } else
                {
                    Clientes.Add(frm.cliente);
                    llenar_Grid();
                }
            }
        }
        private void llenar_Grid()
        {
            dgv1.DataSource = null;
            dgv1.DataSource = Clientes;
        }
    }
}
