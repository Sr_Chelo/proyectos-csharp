﻿using System;

namespace Anidamiento
{
    class Program
    {
        // Creamos una clase llamada Anidada dentro de la clase Program
        public class Anidada
        {
            //Asigno un parámetro
            private string dato;
            //Asigno un método get set
            public string DATO
            {
                get { return dato; }
                set { dato = value; }
            }
        }
        static void Main(string[] args)
        {
            //Inicializo un objeto a partir de la clase Anidada
            Anidada anidada = new Anidada();
            Console.WriteLine("Escriba un dato para la clase anidada");
            //Asigno un dato al parámetro del objeto
            anidada.DATO = Console.ReadLine();
            //Obtengo el dato asignado al parámetro del objeto
            Console.WriteLine("El dato ingresado fue: {0}", anidada.DATO);
        }
    }
}
