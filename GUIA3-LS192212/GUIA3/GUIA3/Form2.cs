﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            cbo1.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            string[] dat = cbo1.Text.Split(' ');
            double value = 0, resp = 0;
            try
            {
                value = Double.Parse(txt1.Text);
                if (cbo1.SelectedIndex == 0)
                {
                    resp = (value * 9 / 5) + 32;
                }
                else if (cbo1.SelectedIndex == 1)
                {
                    resp = (value * 3.281);
                }
                else if (cbo1.SelectedIndex == 2)
                {
                    resp = (value * 2.205);
                }

                lblRes.Text = dat[2] + " = " + resp.ToString();
            }
            catch
            {
                MessageBox.Show(this, "Ingrese solor valores numéricos");
            }
        }

        private void cbo1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] dat = cbo1.Text.Split(' ');
            lblV.Text = "Valor en " + dat[0];
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Login login = new Login();
            if (login.ShowDialog() == DialogResult.OK)
            {
                if (!login.inicio)
                {
                    MessageBox.Show(this, "No se ha logueado");
                    this.Close();
                }
            }
        }
    }
}
