﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA3
{
    public partial class Login : Form
    {
        public bool inicio { get; set; }
        public Login()
        {
            InitializeComponent();
            txtPass.UseSystemPasswordChar = true;
            inicio = false;
        }
        
        private bool validarText(ref TextBox txt)
        {
            bool state = true;
            if(txt.Text == "")
            {
                errorProvider1.SetError(txt, "No puede dejar espacios en blanco");
                state = false;
            } else
            {
                errorProvider1.SetError(txt, "");
                state = true;
            }
            return state;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if(validarText(ref txtUser) && validarText(ref txtPass))
            {
                DialogResult = DialogResult.OK;
                inicio = true;
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void txtUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                button2_Click(null, null);
                e.Handled = true;
            }
        }
    }
}
