﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            rdb1.Checked = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            double percent = 0;
            double sueldo = 0;
            String puesto = "";
            if (rdb1.Checked)
            {
                percent = 0.2;
                puesto = rdb1.Text;
            } else if (rdb2.Checked)
            {
                percent = 0.15;
                puesto = rdb2.Text;
            }
            else if (rdb3.Checked)
            {
                percent = 0.05;
                puesto = rdb3.Text;
            }
            if(txt1.Text.Trim() == "" || txt1.Text.Trim() == null)
            {
                MessageBox.Show(this, "Debe ingresar un nombre");
            } else
            {
                try
                {
                    sueldo = Double.Parse(txt2.Text);
                    lblRes.Text = "Empleado: " + txt1.Text + "\nPuesto: " + puesto + "\nSalario Neto: $" + Math.Round(sueldo, 2) + "\nDescuento: $" + Math.Round(sueldo * percent, 2) + "\nSalario Líquido: $" + Math.Round(sueldo * (1 - percent), 2);
                }
                catch
                {
                    MessageBox.Show(this, "Salario ingresado no válido");
                }
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Login login = new Login();
            if (login.ShowDialog() == DialogResult.OK)
            {
                if (!login.inicio)
                {
                    MessageBox.Show(this, "No se ha logueado");
                    this.Close();
                }
            }
        }
    }
}
