﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIA4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            btnCalcular.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private bool isNum(string data)
        {
            int num = 0;
            try
            {
                num = int.Parse(data);
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        private void btn1_Click(object sender, EventArgs e)
        {
            string texto = "";
            texto = txt1.Text.Trim();
            if(texto != "" && texto != null)
            {
                if (isNum(texto))
                {
                    lst1.Items.Add(texto);
                    txt1.Text = "";
                    txt1.Focus();
                    btnCalcular.Enabled = true;
                } else
                {
                    MessageBox.Show(this, "Ingresar solo datos numéricos");
                }
            } else
            {
                MessageBox.Show(this, "Debe Ingresar datos al campo");
            }
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int parmayorpos = 0,parmayorneg=0, cantimpares = 0, cantidadceros=0,acumimpares=0;
            parmayorpos = int.Parse(lst1.Items[0].ToString());
            parmayorneg = int.Parse(lst1.Items[0].ToString());
            for (int i = 0; i <lst1.Items.Count; i++)
            {
                string dato = lst1.Items[i].ToString();
                int num = int.Parse(dato);

                if(num%2 == 0)
                {
                    if (parmayorpos < num && num>=0)
                    {
                        parmayorpos = num;
                    }

                    if (parmayorneg > num && num < 0)
                    {
                        parmayorneg = num;
                    }
                } else
                {
                    if (num > 0)
                    {
                        cantimpares++;
                        acumimpares += num;
                    }
                }

                if (num == 0)
                {
                    cantidadceros++;
                }
            }

            txt2.Text = parmayorneg.ToString();
            txt3.Text = (((double)cantidadceros / lst1.Items.Count) * 100).ToString();
            txt4.Text = ((double)acumimpares / cantimpares).ToString();
            txt5.Text = parmayorpos.ToString();


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Login login = new Login();
            if (login.ShowDialog() == DialogResult.OK)
            {
                if (!login.inicio)
                {
                    MessageBox.Show(this, "No se ha logueado");
                    Application.Exit();
                    this.Close();
                }
            }
        }

        private void txt1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == (int)Keys.Enter)
            {
                btn1_Click(null,null);
                e.Handled = true;
            }
        }
    }
}
