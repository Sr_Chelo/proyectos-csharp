﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Desafio1
{
    public partial class Form2 : Form
    {
        string[] categorias = { "Belleza", "Bienestar", "Prevención", "Medicamentos" };
        string[] tipo = { "Venta Libre", "Bajo Prescripción" };
        public Producto producto;

        public bool correcto { get; set; }
        public Form2()
        {
            InitializeComponent();
            cboCat.DataSource = categorias;
            cboT.DataSource = tipo;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            producto = new Producto();
            errorProvider1.Clear();
            bool state = true;
            if (!producto.addNombre(txtName.Text))
            {
                state = false;
                errorProvider1.SetError(txtName, "Nombre Ingresado no valido");
            }
            if (!producto.addPrecio(txtPrecio.Text))
            {
                state = false;
                errorProvider1.SetError(txtPrecio, "Precio Ingresado no valido");
            }
            if (state)
            {
                producto.Categoria = cboCat.Text;
                producto.Tipo = cboT.Text;
                DialogResult = DialogResult.OK;
                correcto = true;
                MessageBox.Show(this, "Producto Ingresado con éxito");
                this.Close();
            }
        }
    }
}
