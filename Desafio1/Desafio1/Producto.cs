﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Desafio1
{
    public class Producto
    {
        private int codigo;
        private string nombre;
        private double precio;
        private string tipo;
        private string categoria;

        public int Codigo
        {
            get { return this.codigo; }
            set { codigo = value + 1000; }
        }

        public string Nombre
        {
            get { return nombre; }
        }

        public bool addNombre(string text)
        {
            if (!validate_text(text))
            {
                return false;
            }
            this.nombre = text;
            return true;
        }

        public double Precio
        {
            get { return precio; }
        }
        public bool addPrecio(string text)
        {
            if (!validate_number(text))
            {
                return false;
            }
            double num = Double.Parse(text);
            if (num <= 0)
            {
                return false;
            }
            this.precio = num;
            return true;
        }

        public string Tipo
        {
            get { return tipo; }
            set { this.tipo = value; }
        }

        public string Categoria
        {
            get { return categoria; }
            set { this.categoria = value; }
        }

        private bool validate_text(string text)
        {
            if (text.Trim() == "" || text.Trim() == null)
            {
                return false;
            }
            return true;
        }
        private bool validate_number(string text)
        {
            if (!validate_text(text))
            {
                return false;
            }

            try
            {
                double num = Double.Parse(text);
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
