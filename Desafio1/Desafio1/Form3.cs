﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Desafio1
{
    public partial class Form3 : Form
    {
        string tipopago = "";
        List<Carrito> carrito;
        List<Producto> ProductodGlobales;
        public bool correcto { get; set; }
        public Form3(List<Carrito> carr = null, List<Producto> prod = null)
        {
            InitializeComponent();
            carrito = carr;
            ProductodGlobales = prod;
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            Form4 frm = new Form4();
            if(frm.ShowDialog() == DialogResult.OK)
            {
                if (frm.correcto)
                {
                    tipopago = frm.data;
                } else
                {
                    MessageBox.Show(this, "No se ha establecido un método de pago");
                    this.Close();
                }
            }
            llenar_line();
            llenar_texto1("Farmacia X");
            llenar_texto1("Esquina Formada entre 51 Av. Norte No. 2636, Colonia Flor Blanca, El Salvador ");
            llenar_texto1("San Salvador,San Salvador");
            llenar_texto1("Tel: 25103467");
            llenar_texto1("Fecha " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            llenar_line();
            llenar_texto1("Esta Venta");
            llenar_texto1("Producto                 Cant   Precio");
            double sum = 0;
            int lventa = 0, rventa = 0;
            for (int i = 0; i < carrito.Count; i++)
            {
                Producto prod = ProductodGlobales.FirstOrDefault(x => x.Codigo == carrito[i].Codigo);
                llenar_texto2(carrito[i].Nombre, carrito[i].Cantidad.ToString(), prod.Precio.ToString());
                sum += carrito[i].Cantidad * prod.Precio;
                if (prod.Tipo == "Bajo Prescripción")
                {
                    rventa++;
                }
                else if (prod.Tipo == "Venta Libre")
                {
                    lventa++;
                }

            }
            llenar_texto1();
            llenar_texto3("P. de Venta Libre", "", lventa.ToString());
            llenar_texto3("P. Bajo Prescripción", "", rventa.ToString());
            llenar_line2();
            llenar_texto3("Sub Total","$",sum.ToString());
            llenar_texto3("IVA", "$", (Math.Round(sum * 0.13,2)).ToString());
            llenar_texto3("Total", "$", (Math.Round(sum * 1.13, 2)).ToString());
            llenar_texto3("Método de págo", "", tipopago);
            llenar_texto1();
            llenar_line();
        }
        private void llenar_line()
        {
            string data = "";
            for (int i = 0; i < 46; i++)
            {
                data += "=";
            }            
            lstB1.Items.Add(data);
        }
        private void llenar_line2()
        {
            string data = "";
            data += "  ";
            for (int i = 0; i < 42; i++)
            {
                data += "-";
            }
            data += "  ";
            lstB1.Items.Add(data);
        }

        private void llenar_texto1(string text = " ")
        {
            text = text.Replace(" ", "_");
            string[] words = (from Match m in Regex.Matches(text, @"[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ_:;,.*\/ ]{1,40}") select m.Value).ToArray();
            for(int i = 0; i < words.Length; i++)
            {
                words[i] = words[i].Replace("_", " ");
            }
            string data = "";
            int rex = 0;
            for (int i = 0; i < words.Length; i++)
            {
                rex = 0;
                data = " ";
                int medium = words[i].Length / 2;
                if (words[i].Length % 2 != 0)
                {
                    medium-=2;
                    rex = 2;
                }
                for(int j = 0; j < 22-medium-rex; j++)
                {
                    data += " ";
                }
                data += words[i];
                if (words[i].Length % 2 != 0)
                {
                    rex = 3;
                }
                for (int j = 0; j < 22 - medium-rex; j++)
                {
                    data += " ";
                }
                data += " ";                
                lstB1.Items.Add(data);
            }

        }
        private void llenar_texto3(string text, string text1,string text2)
        {
            string data = "  ";
            for(int i = 0; i < 25-(text.Length); i++)
            {
                data += " ";
            }
            data+= text;
            for(int i = 0; i < 5; i++)
            {
                data+=" ";
            }
            for(int i=0;i<11-(text1+""+text2 + "  ").Length; i++)
            {
                data += " ";
            }
            data += text1;
            data += text2 + "  ";
            lstB1.Items.Add(data);

        }
        private void llenar_texto2(string text,string text2,string text3)
        {
            string[] words = (from Match m in Regex.Matches(text, @"[a-zA-Z0-9áéíóúÁÉÍÓÚ_:;,.* ]{1,25}") select m.Value).ToArray();
            string data = "";
            for(int i = 0; i < words.Length; i++)
            {
                data= "  ";
                data += words[i];
                if (i == words.Length - 1)
                {
                    for(int j = 0; j < (23 - words[i].Length); j++)
                    {
                        data += " ";
                    }
                    data += "  ";
                    for(int j = 0; j < (6 - text2.Length); j++)
                    {
                        data += " ";
                    }
                    data += text2;
                    data += " x ";
                    data += "$"+text3;
                    for (int j = 0; j < (7 - text3.Length); j++)
                    {
                        data += " ";
                    }
                } else
                {
                    for(int j =0;j< (42- words[i].Length); j++)
                    {
                        data += " ";
                    }
                }
                data+="  ";
                lstB1.Items.Add(data);
            }
        }
        //          {2}+{25}+{2}+{6}+{1}+{8}+{2}
        //           # cadena#########cantidad*precio #

        private void button4_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            correcto = false;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            correcto = false;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            correcto = true;
            MessageBox.Show(this, "Gracias por su compra, vuelva pronto");
            this.Close();
        }
    }
}
