﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace Desafio1
{
    public partial class Form1 : Form
    {
        List<Producto> ProductosGlobales = new List<Producto>();
        List<Carrito> carrito;
        int cantcarrito = 0;
        string[] categorias = { "Todas", "Belleza", "Bienestar", "Prevención", "Medicamentos" };
        int last = 0;
        public Form1()
        {
            InitializeComponent();
            string[] productos = { "Shampoo","Acondicionador","Crema Corporal","Crema Facial","Kit para mani-pedicuras","kit-ortodoncia","Dsodorante","Salud Intima","Preservativos","Lubricantes","Mascarilla Quirúrgicaguantes","Jabón Antibacterial","Amonio","Mascara Kareta","Aspirina","Loratadina","Acetaminofén","Ibuprofeno","Dorival","Curitas","Complejo B","Vitamina A","Vitamina B","Vitamina C","Crema Cicatrizante","Alcohol 40","Alcohol 60","Alcohol 90","Panadol Antigripal","Panadol Multisintomas","Kamillosan","Gasa","Esparadrapo","Paracetamol" };
            string[] tipo = { "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Venta Libre", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción", "Bajo Prescripción"};
            string[] categoria = { "Belleza", "Belleza", "Belleza", "Belleza", "Belleza", "Bienestar", "Bienestar", "Bienestar", "Bienestar", "Bienestar", "Prevención", "Prevención", "Prevención", "Prevención", "Prevención", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos", "Medicamentos" };
            var seed = Environment.TickCount;
            var random = new Random(seed);
            for (int i = 0; i < productos.Length; i++)
            {
                Producto prod = new Producto();
                prod.Codigo = i;
                prod.addNombre(productos[i]);
                prod.addPrecio((random.Next(0, 10)+0.99).ToString());
                prod.Tipo = tipo[i];
                prod.Categoria = categoria[i];
                ProductosGlobales.Add(prod);
                last = i;
            }
            this.llenar_grid();
            cboCat.DataSource = categorias;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void llenar_grid(List<Producto> Productos = null)
        {
            dgv1.DataSource = null;
            if (Productos == null)
            {
                dgv1.DataSource = ProductosGlobales;
            }
            else {
                dgv1.DataSource = Productos;
            }
        }
        private void llenar_listBox()
        {
            lstB1.Items.Clear();
            if(carrito != null)
            {
                for (int i = 0; i < carrito.Count; i++)
                {
                    lstB1.Items.Add(carrito[i].Cantidad + "  -  " + carrito[i].Nombre);
                }
            }
        }

        private void cboCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            string cat = categorias[cboCat.SelectedIndex];
            if (cat != "Todas")
            {
                List<Producto> productos = new List<Producto>();
                for (int i = 0; i < ProductosGlobales.Count; i++)
                {
                    if (ProductosGlobales[i].Categoria == cat)
                    {
                        productos.Add(ProductosGlobales[i]);
                    }
                }
                this.llenar_grid(productos);
            } else
            {
                this.llenar_grid();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cantcarrito == 15)
            {
                MessageBox.Show(this, "Solo puede solicitar 15 productos por venta");
            } else
            {
                if (ProductosGlobales.Count() != 0)
                {
                    int codigo = int.Parse(dgv1.Rows[dgv1.CurrentCell.RowIndex].Cells[0].Value.ToString());
                    Producto prod = ProductosGlobales.FirstOrDefault(x => x.Codigo == codigo);
                    if (carrito != null)
                    {
                        bool state = true;
                        for (int i = 0; i < carrito.Count; i++)
                        {
                            if (carrito[i].Codigo == prod.Codigo)
                            {
                                carrito[i].Cantidad++;
                                state = false;
                                cantcarrito++;
                            }
                        }
                        if (state)
                        {
                            Carrito car = new Carrito();
                            car.Codigo = prod.Codigo;
                            car.Nombre = prod.Nombre;
                            car.Cantidad = 1;
                            carrito.Add(car);
                            cantcarrito++;
                        }
                    }
                    else
                    {
                        carrito = new List<Carrito>();
                        Carrito car = new Carrito();
                        car.Codigo = prod.Codigo;
                        car.Nombre = prod.Nombre;
                        car.Cantidad = 1;                        
                        carrito.Add(car);
                        cantcarrito++;
                    }
                    this.llenar_listBox();
                }
            }

        }



        private void button5_Click(object sender, EventArgs e)
        {
            int index = lstB1.SelectedIndex;
            if (index >= 0)
            {
                if (carrito[index].Cantidad == 1)
                {
                    carrito.RemoveAt(index);
                    index--;
                } else
                {
                    carrito[index].Cantidad--;
                }
                cantcarrito--;
                this.llenar_listBox();
                if (carrito.Count != 0)
                {
                    lstB1.SelectedIndex = index;
                }
            }
            else
            {
                MessageBox.Show(this, "No ha seleccionado productos del carrito");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if(ProductosGlobales.Count() != 0)
            {
                int codigo = int.Parse(dgv1.Rows[dgv1.CurrentCell.RowIndex].Cells[0].Value.ToString());
                Producto prod = ProductosGlobales.FirstOrDefault(x => x.Codigo == codigo);
                ProductosGlobales.Remove(prod);
                try
                {
                    Carrito car = carrito.FirstOrDefault(x => x.Codigo == codigo);
                    if (carrito.FirstOrDefault(x => x.Codigo == codigo) != null)
                    {
                        cantcarrito -= car.Cantidad;
                        carrito.Remove(car);
                    }
                    llenar_listBox();
                }
                catch
                {
                }
                llenar_grid();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 frm = new Form2();
            if(frm.ShowDialog() == DialogResult.OK)
            {
                if (!frm.correcto)
                {
                    MessageBox.Show("No se ha ingresado el producto");
                }else
                {
                    last++;
                    frm.producto.Codigo = last;
                    ProductosGlobales.Add(frm.producto);
                    llenar_grid();
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if(carrito != null && carrito.Count() != 0)
            {
                Form3 frm = new Form3(carrito, ProductosGlobales);
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    if (frm.correcto)
                    {
                        carrito = null;
                        cantcarrito = 0;
                        llenar_listBox();
                    }
                }
            }
            else
            {
                MessageBox.Show(this, "No ha agregado productos al carrito","Error");
            }
        }
    }
}
