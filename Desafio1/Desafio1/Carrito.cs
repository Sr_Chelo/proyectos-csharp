﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Desafio1
{
    public class Carrito
    {
        private int codigo;
        private string nombre;
        private int cantidad;

        public int Codigo
        {
            get { return codigo; }
            set { codigo = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public int Cantidad
        {
            get { return cantidad; }
            set { cantidad = value; }
        }
    }
}
