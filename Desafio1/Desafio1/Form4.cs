﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Desafio1
{
    public partial class Form4 : Form
    {
        public string data { get; set; }
        public bool correcto { get; set; }
        public Form4()
        {
            InitializeComponent();
            string[] types = { "Efectivo", "Tarjeta" };
            cboCat.DataSource = types;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            data = cboCat.Text;
            DialogResult = DialogResult.OK;
            correcto = true;
            this.Close();
        }
    }
}
