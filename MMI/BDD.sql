USE [master]
GO
IF (SELECT count(*) FROM DBO.SYSDATABASES WHERE NAME = 'MMI') = 0
BEGIN
	CREATE DATABASE MMI
	USE [MMI]	
	CREATE TABLE Admin(
		ID INT IDENTITY(1,1) PRIMARY KEY,
		Nombre nvarchar(255) not null,
		Email nvarchar(255) NOT NULL,
		Password nvarchar(255) not null
	)
END
