﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMI
{
    public partial class Main : Form
    {
        LINQ_TO_SQLDataContext con = new LINQ_TO_SQLDataContext();
        public Admin admin = null;
        public Main()
        {
            InitializeComponent();
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Main_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Hide();
            MessageBox.Show("Gracias por Usar el programa");
            this.Close();
            Application.Exit();
        }

        private void btnMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Login();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Login();
        }


        private void Login()
        {
            admin = null;
            Login frm = new Login();
            this.Hide();
            if (frm.ShowDialog() == DialogResult.OK)
            {
                if (frm.correct == true)
                {
                    admin = frm.admin;
                    this.Show();
                    lblNombre.Text = admin.Nombre;
                } else
                {
                    MessageBox.Show(this, "No se a logueado ningún admin");
                    this.Close();
                }
            }
        }
    }
}
