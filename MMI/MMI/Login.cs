﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MMI
{
    public partial class Login : Form
    {
        LINQ_TO_SQLDataContext con = new LINQ_TO_SQLDataContext();
        public Admin admin = null;
        public bool correct { get; set; }
        public Login()
        {
            InitializeComponent();
            txtPass.PasswordChar = '•';
        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            correct = false;
            this.Close();
        }

        private void btnMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PN1.BackColor = Color.FromArgb(99, 219, 75);
            PN2.BackColor = Color.FromArgb(99, 219, 75);
            errorProvider1.Clear();
            var Admin = (from Adm in con.Admin
                        where Adm.Email == txtEmail.Text
                        select Adm).FirstOrDefault();
            if(Admin != null)
            {
                string myPassword = txtPass.Text;
                string mySalt = BCrypt.Net.BCrypt.GenerateSalt();
                string myHash = BCrypt.Net.BCrypt.HashPassword(myPassword, mySalt);
                bool doesPasswordMatch = BCrypt.Net.BCrypt.Verify(myPassword, Admin.Password);
                if (doesPasswordMatch)
                {
                    this.correct = true;
                    admin = Admin;
                    DialogResult = DialogResult.OK;
                    MessageBox.Show(this, "Admin loguedo correctamente");
                    this.Close();
                }
                else
                {
                    errorProvider1.SetError(txtPass, "La contraseña ingresada no es correcta");
                    PN2.BackColor = Color.Red;
                }
            } else
            {
                errorProvider1.SetError(txtEmail, "El correo ingresado no coincide con nuestros registros");
                PN1.BackColor = Color.Red;
            }
        }
    }
}
